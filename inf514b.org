* Idées

- greedy: Prim
  - maximiser taille du premier groupe, puis itérer avec le reste
- greedy: Kruskal
  - ajouter points un par un
- heuristiques

- théorie des graphes: sous-graphes complets
- coloration de graphe
  - polynomial 

- line graph: edge i connected to edge j if intersection
  - equivalent find partition of points into colors, such that each color not adjacent to other same color
    --> add distance: distance between edges?

- topological features: plonger dans espace de dimension k --> appliquer algo distances, ...
  
- champs vectoriels

- prendre k donné, regarder faisabilité : binary search
- divide and conquer
  

- programmation lineaire
- programmation dynamique
  
- algorithmes évolutifs
- machine learning: domaine continu

* Line Graph

- consider line graph
- Disconnected components: independent subproblems
- Articulated points: independent subproblems (num coloration >= 2 as soon as n >= 2)
--> graph of min degree of 2 
- assignation m = 2: profondeur modulo 2
--> si oui: gagné
- *m >= 3*
--> remove all equidistant edge extremities to root
  - if *m = 3*, take graph formed by these nodes + edges: find minimum covering of edges: if tree (linear), else NP-complete 2 approx

- >= max degree clique
- greedy: <= degre max + 1 (https://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/)
--> Heuristic: add one color for high degree nodes (star shapes)

Euler theorem: Let G be a connected simple planar graph with $e$ edges and $v$ vertices. Then the number of regions $r$ in the graph is equal to $e-v+(k+1)$
- Corollary: If G is a connected planar graph with $e$ edges and $v$ vertices, where $v\geq 3$, then $e\leq 3v - 6$. Also G cannot have a vertex of degree exceeding $5$.

increasing mex algo: https://www.geeksforgeeks.org/m-coloring-problem-backtracking-5/
  - decreasing mex pass?

* Line Graph Complement 


- combine both outputs: one step each


* SPSA
- represent as stochastic process: weight distribution take node: take color mex
  --> try to minimize expectation of number of colors
  --> stochastic gradient descent without gradient 
- algorithm: stochastic approximation
  - SPSA


* Perturbations
- petites perturbations autour point
