#pragma once

#include <nlohmann/json.hpp>
#include <random>
#include <string>
#include <fstream>
#include <vector>
#include <deque>
#include <algorithm>

using namespace std;
using json = nlohmann::json;

using vi = vector<int>;

//https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
std::string get_file_contents(const string filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return(contents);
  }
  throw("Error reading file");
}

bool write_to_file(const string filename, const string content)
{
  std::ofstream out(filename, std::ios::out | std::ios::binary);
  if (out)
  {
    out.write(content.c_str(), content.size());
    out.close();
    return true;
  }
  throw(errno);
}

// https://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
template <typename T>
vector<int> sort_indexes(const vector<T> &v) {

  // initialize original index locations
  vector<int> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  // using std::stable_sort instead of std::sort
  // to avoid unnecessary index re-orderings
  // when v contains elements of equal values 
  //stable_sort(execution::par_unseq, idx.begin(), idx.end(),
  stable_sort(idx.begin(), idx.end(),
       [&v](int i1, int i2) {return v[i1] < v[i2];});

  return idx;
}


vector<int> irange(int i, int j = 0) {
  if(j < i)
    return irange(j, i);
  if(i == j)
    return {};
  vector<int> v(j-i);
  for(int k = 0; k < j-i; k++)
    v[k] = i+k;
  return v;
}

vector<int> permutation(int n) {
  auto v = irange(n);
  random_shuffle(v.begin(), v.end());
  return v;
}

template <typename T>
vector<int> sort_indexes_decreasing(const vector<T> &v) {
  vector<int> idx(v.size());
  iota(idx.begin(), idx.end(), 0);
  stable_sort(idx.begin(), idx.end(),
       [&v](int i1, int i2) {return v[i1] > v[i2];});

  return idx;
}
// using Point = glm::dvec2;

using ftype = double;
struct Point {
  ftype x,y;
};

inline Point operator+(const Point &A, const Point &B) {
  return {A.x + B.x, A.y + B.y};
}
inline Point operator-(const Point &A, const Point &B) {
  return {A.x - B.x, A.y - B.y};
}

inline ftype cross(Point A, Point B) { return A.x * B.y - A.y * B.x; }
inline ftype dot(Point A, Point B) { return A.x * B.x + A.y * B.y; }

//https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
bool segmentsIntersect(Point A, Point B, Point C, Point D)
{
  Point p = A, q = C, r = B-A, s = D-C;

  // parallel
  if (cross(r, s) == 0) {
    // colinear
    if (cross(q - p, r) == 0) {
      ftype _t0 = dot(q-p, r) / dot(r,r);
      ftype _t1 = dot(q-p+s, r) / dot(r,r);
      ftype t0 = min(_t0, _t1), t1 = max(_t0, _t1);
      // overlapping or disjoint
      return !(t1 <= 0 || t0 >= 1);
    }
    // parallel non intersecting
    return false; 
  }

  // not parallel
  ftype t = cross(q-p, s) / cross(r, s);
  ftype u = cross(q-p, r) / cross(r, s);
  // intersect or not
  if((t == 0 || t == 1) && (u == 0 || u == 1))
    return false;
  return (0 <= t && t <= 1 && 0 <= u && u <= 1);
}
