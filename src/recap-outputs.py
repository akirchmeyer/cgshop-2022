import json
import os
from os import listdir
from os.path import isfile, join
import time
import numpy as np

output_path = '../data/instances/outputs2'
instance_path = '../data/instances'

outputs = [join(output_path, f) for f in listdir(output_path)]
outputs = [f for f in outputs if isfile(f)]
n_outputs = len(outputs)

print('\t i\t\t name\t\t\tn_color\tn_verts\tn_edges')

entries = []
for i, filename in enumerate(outputs):
    name = os.path.splitext(os.path.splitext(os.path.basename(filename))[0])[0]
    n_colors = json.load(open(filename, 'rb'))['num_colors']

    instance_filename = os.path.join(instance_path, f'{name}.instance.json')
    instance = json.load(open(instance_filename, 'rb'))
    n, m = instance['n'], instance['m']
    entries.append((i, name, n_colors, n, m))

ns = np.array([entry[3] for entry in entries])
ms = np.array([entry[4] for entry in entries])
colors = np.array([entry[2] for entry in entries])
idx = np.argsort(colors)
for k in idx:
    i, name, n_colors, n, m = entries[k]

    print(f'\t{i}/{n_outputs}\t\t{name}\t\t{n_colors}\t{n}\t{m}')

import pandas as pd

print(pd.DataFrame(colors, columns=['n_colors']).describe())

import matplotlib.pyplot as plt

plt.scatter(ns, np.log(colors))
plt.scatter(ms, np.log(colors))
#plt.plot(np.sort(colors))
plt.show()
