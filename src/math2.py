import json
import os
import numpy as np

from cgshop2022utils.verify import verify_coloring
from cgshop2022utils.io import read_instance, read_solution

from os import listdir
from os.path import isfile, join
for i,f in enumerate([f for f in listdir('../data/instances/') if isfile(join('../data/instances', f))]):
    instanceName = os.path.splitext(os.path.splitext(f)[0])[0]#'reecn11799'
    inFileName = "../data/instances/" + instanceName + ".instance.json"
    outFileName = "../data/instances/outputs/" + instanceName + ".solution.json"
    if not os.path.exists(outFileName):
        continue

    instance = read_instance(inFileName)
    graph = instance['graph']
    solution = read_solution(outFileName)
    error, num_colors = verify_coloring(graph, solution['colors'], expected_num_colors=solution['num_colors'])
    degrees = graph.degree()
    s_deg = np.sum([deg for node, deg in degrees])
    m = graph.number_of_edges()
    c = num_colors
    bound = int(2*s_deg**0.5)
    
    print(f"{i}\t{m}\t{s_deg}\t{bound}\t{c}\t{bound-c}")
