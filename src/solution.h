#pragma once
#include <chrono>
#include <random>
#include "line-graph.h"


struct Solution {
  Solution(LineGraph &lgraph)
      : lineGraph(lgraph) {
    n = lgraph.n;
    colors.resize(n, 0);
  }

  int n;
  const LineGraph &lineGraph;
  vector<int> colors;
  

  int nColors() const {
    int nc = 0;
    for (int i = 0; i < n; i++) 
      nc = max(nc, colors[i]); 
    return nc;
  }

  vector<int> getDistributionColors() const {
    int nc = nColors();
    vector<int> buckets(nc+1, 0);
    for (int i = 1; i < n; i++) {
      assert(colors[i] > 0);
      buckets[colors[i]]++;
    }
    return buckets;
  }

  vector<int> getDistributionSizeColors() const {
    vector<int> distrib = getDistributionColors();
    int nc = nColors();

    vector<int> buckets(n+1, 0);
    for (int i = 0; i <= nc; i++) 
      buckets[distrib[i]]+=distrib[i];
    return buckets;
  }

  void recolor() {
    int nC = nColors();
    vector<vector<int>> distrib(nC+1);
    for(int i = 0; i < n; i++)
      distrib[colors[i]].push_back(i);
    vector<int> sizes(nC+1, 0);
    for(int i = 0; i < nC+1; i++)
      sizes[i] = distrib[i].size();
    auto indices = sort_indexes_decreasing(sizes);
    for (int k = 0; k < nC; k++) 
      for(int i : distrib[indices[k]])
	colors[i] = k+1;
  }

  void reflow(vector<int> const &order) {
    const auto &adj = lineGraph.adj;
    const auto &links = lineGraph.links;
    const auto &degrees = lineGraph.degrees;

    int nc = lineGraph.d_max+2;
    vector<int> taken(nc, -1);
    for (int i : order) {
      ITER_ADJ(i, j) {
        // for(int j : adj[i])
        taken[colors[j]] = i;
      }
      int c = 1;
      while(taken[c] == i)
        c++;
      colors[i] = c;
    }
  }

  void rematch(vector<int> const &order, bool activateAll = true) {
    int C = nColors();
    const auto &c_adj = lineGraph.c_adj;
    const auto &links = lineGraph.links;
    const auto &degrees = lineGraph.degrees;
    // generate cliques from colors
    vector<vector<int>> cliques(C+1);
    // number of elements per color
    vector<int> distrib(C+1, 0);
    // temporary color of elements of a given clique
    vector<int> tmpc(n, 0);

    for(int i = 0; i < n; i++)
      cliques[order[colors[i]-1]+1].push_back(i), colors[i] = 0;

    // for every clique
    for(int l = C, c = 1; l >= 1; l--) {
      bool allUsed = false, used = false;
      for (int i : cliques[l]) {
        vector<int> adj(c, 0);
	ITER_C_ADJ(i, j) {
        //for (int j : c_adj[i]) {
	  int cj = colors[j];
          if (cj > 0 && (++adj[cj]) == distrib[cj]) // && (tmpc[i] == 0 || distrib[cj] > distrib[tmpc[i]]))
            tmpc[i] = cj;
        }
        if (tmpc[i] == 0) 
          tmpc[i] = c, used = true;
	if(activateAll && used)
	  allUsed = true;
      }
      for (int i : cliques[l])
        colors[i] = allUsed ? c : tmpc[i], distrib[colors[i]]++;
      if(allUsed || used)
	++c;
    }
  }

  vector<int> generateOrderNumDiffColors() {
    const auto &adj = lineGraph.adj;
    const auto &links = lineGraph.links;
    const auto &degrees = lineGraph.degrees;

    int nc = lineGraph.d_max+2;
    vector<int> taken(nc, -1), res(n, -1);
    for (int i = 0; i < n; i++) {
      int nc = 0;
      //for(int j : adj[i])
      ITER_ADJ(i, j) 
        nc += (taken[colors[j]] != i), taken[colors[j]] = i;
      res[i] = nc;
    }
    return res;
  }

  void runAlternatingReflowRematch(const std::string fileName) {
    // initial flow
    reflow(sort_indexes_decreasing(lineGraph.degrees));
    int nc0 = nColors();
    printf("initial flow: %d [%s]\n", nc0, lineGraph.isValid(colors) ? "VALID" : "INVALID");
    int mc = nc0;
    for (int i = 0; i < 200; i++) {
      recolor();
      //rematch(permutation(nColors()), true);
      rematch(irange(nColors()));
      int nc = nColors();
      mc = min(nc, mc);

      if((i+1)%100 == 0)
        printf("rematch solution i=%d: %d %d (%.2f%%) [%s]\n", i, nc, mc, double(100 * nc) / nc0, lineGraph.isValid(colors) ? "VALID" : "INVALID");
      else
        printf("rematch solution i=%d: %d %d (%.2f%%)\n", i, nc, mc, double(100 * nc) / nc0);

      reflow(permutation(n));
    }
    saveToFile(fileName);
  }

  vi generateOrderDegrees() {
    return sort_indexes_decreasing(lineGraph.degrees);
  }

  void saveToFile(const string fileName) {
    
    json sol = {{"type", "Solution_CGSHOP2022"},
                {"instance", lineGraph.instanceName},
                {"num_colors", nColors()},
                {"colors", colors}};

    string solution = sol.dump(4);
    write_to_file(fileName, solution);
  }
};
