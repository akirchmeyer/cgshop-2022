#include "util.h"
#include "line-graph.h"
#include "solution.h"

int main(int argc, char* argv[]) {
  srand(time(0));
  //const string instanceName = (argc == 1) ? "1-FullIns_3" : argv[1];
  //const string inFileName = "../data/GGC/" + instanceName + ".col";
  const string instanceName = (argc == 1) ? "reecn11799" : argv[1];
  const string instanceType = (argc < 3) ? "instances" : argv[2];
  const string instanceExt = (instanceType != "GGC") ? "instance.json" : "col";
  
  const string inFileName = "../data/" + instanceType + "/" + instanceName + "." + instanceExt;
  const string outFileName = "../outputs4/" + instanceName + ".solution.json";
  ifstream in(outFileName);
  if (in) {
    printf("%s already computed: exiting\n", outFileName.c_str());
    return 0;
  }

  ofstream out(outFileName);
  out << std::endl;
  out.close();
  

  LineGraph lineGraph;
  if(argc >= 3 && string(argv[2]) == "GGC")
    lineGraph.readFromGGC(inFileName);
  else
    lineGraph.readFromInstance(inFileName);
  lineGraph.createLinks();
  printf("Read file: %s n=%d d_max=%d\n", inFileName.c_str(), lineGraph.n, lineGraph.d_max);
  Solution solution(lineGraph);
  solution.runAlternatingReflowRematch(outFileName);

  return 0;
}

