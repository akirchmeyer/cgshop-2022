import os
import subprocess as sp
from os import listdir
from os.path import isfile, join
import time
import random

path = '../data/instances'
instances = [join(path, f) for f in listdir(path)]
instances = [f for f in instances if isfile(f)]
random.shuffle(instances)

n = len(instances)
for i, instance in enumerate(instances):
    name = os.path.splitext(os.path.splitext(os.path.basename(instance))[0])[0]
    print(f'RUNNING FOR i={i}/{n} instance={name}')
    start = time.time()
    sp.run(f'./cgshop {name}', shell=True)
    print(f'Time elapsed: {time.time()-start}')
    print()
    print()
