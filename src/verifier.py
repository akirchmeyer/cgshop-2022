import os



from cgshop2022utils.verify import verify_coloring
from cgshop2022utils.io import read_instance, read_solution


for i in range(1,7):
    instanceName = f'sqrm_5K_{i}'
    inFileName = "../data/cgshop_2022_examples_01/example-instances-sqrm/" + instanceName + ".instance.json"
    outFileName = "../data/cgshop_2022_examples_01/example-instances-sqrm/" + instanceName + ".solution.json"

    instance = read_instance(inFileName)
    graph = instance['graph']
    solution = read_solution(outFileName)
    error, num_colors = verify_coloring(graph, solution['colors'], expected_num_colors=solution['num_colors'])
    print(i, error, num_colors)
