#pragma once
#include "util.h"
#include <atomic>
#include <algorithm>
//#include <execution>

using namespace std;

//#define ITER_ADJ(i, j) for(int i##j = 0, j = links[i##j*(n-1)]; i##j < degrees[i]; ++i##j, j = links[i*(n-1)+i##j])
//#define ITER_C_ADJ(i, j) for(int i##j = n-2, j = links[i*(n-1)+i##j]; i##j >= degrees[i]; --i##j, j = links[i*(n-1)+i##j])
#define ITER_ADJ(i, j) for(int j : adj[i])
#define ITER_C_ADJ(i, j) for(int j : c_adj[i])

struct LineGraph {
  LineGraph() {}
  vector<vector<int>> adj;
  vector<vector<int>> c_adj;
  vector<int> links; 
  vector<int> degrees, c_degrees;
  int n, d_max, c_d_max;

  int N, M;
  vector<Point> points;
  vector<vector<int>> edges;
  vector<int> edges_i, edges_j;

  string instanceName;

  int isValid(vector<int> colors) const {
    if(instanceName == "")
      return true;
    int nc = 0; 
    for(int i = 0; i < M; i++)
      nc = max(nc, colors[i]);
    vector<int> parts[nc];
    for (int i = 0; i < M; i++) {
      assert(colors[i] > 0);
      parts[colors[i]-1].push_back(i);
    }
    for (int k = 0; k < nc; k++) {
      for (int i : parts[k]) {
        for (int j : parts[k]) {
          if (i == j)
            continue;
          int u = edges_i[i], v = edges_j[i];
          int x = edges_i[j], y = edges_j[j];
          if (segmentsIntersect(points[u], points[v], points[x], points[y]))
            return false;
        }
      }
    }
    return true;
  }

  void readFromInstance(const string &fileName) {
    const string rawData = get_file_contents(fileName);
    json data = json::parse(rawData);

    N = data["n"], M = data["m"];
    instanceName = data["id"];
    //printf("Read \"%s\" N=%d M=%d\n", instanceName.c_str(), N, M);

    points.resize(N);
    edges.resize(N);
    edges_i.resize(M);
    edges_j.resize(M);

    for(int i = 0; i < N; i++)
	points[i] = {(ftype)data["x"][i], (ftype)data["y"][i]};

    for (int i = 0; i < M; i++) {
      edges_i[i] = data["edge_i"][i], edges_j[i] = data["edge_j"][i];
      edges[edges_i[i]].push_back(edges_j[i]);
      edges[edges_j[i]].push_back(edges_i[i]);
    }
    n = M;
    adj.resize(n);
    c_adj.resize(n);
    degrees.resize(n);
    c_degrees.resize(n);

    //auto range = irange(0,n);
    //for_each(execution::par_unseq, range.begin(), range.end(), [&](int i) {
    for(int i = 0; i < n; i++) {
      int u = edges_i[i], v = edges_j[i];
      for (int j = 0; j < n; j++) {
	if(i == j)
	  continue;
        int x = edges_i[j], y = edges_j[j];
        if (segmentsIntersect(points[u], points[v], points[x], points[y]))
          adj[i].push_back(j);
        else
          c_adj[i].push_back(j);
      }
      degrees[i] = adj[i].size();
      c_degrees[i] = c_adj[i].size();
    }

    d_max = 0, c_d_max = 0;
    for (int i = 0; i < n; i++)
      d_max = max(d_max, degrees[i]), c_d_max = max(c_d_max, c_degrees[i]);
  }

  void readFromGGC(const string &fileName) {
    ifstream file(fileName);
    int u, v;
    string s, s2;
    char c;

    vector<vector<int>> A;
    
    n = 0;
    while((file >> c)) {
      if(c == 'c') {
        getline(file, s);
        printf("ignored: %s\n", s.c_str());
        continue;
      }
      if(c == 'p') {
        file >> s;
        file >> u;
        file >> v;
	n = u;
        A.resize(n, vector<int>(n, 0));
      }
      if(c == 'e') {
        file >> u >> v;
        A[u-1][v-1] = 1, A[v-1][u-1] = 1;
      }
    }

    printf("n = %d\n", n);
    adj.resize(n);
    c_adj.resize(n);
    degrees.resize(n);
    c_degrees.resize(n);

    for(int i = 0; i < n; i++) {
      for(int j = i+1; j < n; j++) {
        if(A[i][j])
          adj[i].push_back(j), adj[j].push_back(i);
        else
          c_adj[i].push_back(j), c_adj[j].push_back(i);
      }
    }

    d_max = 0, c_d_max = 0;
    for (int i = 0; i < n; i++) 
      degrees[i] = adj[i].size(), c_degrees[i] = c_adj[i].size(), d_max = max(d_max, degrees[i]), c_d_max = max(c_d_max, c_degrees[i]);
    printf("d_max=%d\n", d_max);
    printf("c_d_max=%d\n", d_max);
  }

  void createLinks() {
    links.resize(n*(n-1));
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < degrees[i]; j++)
        links[i * (n-1) + j] = adj[i][j];

      for (int j = 0; j < c_degrees[i]; j++)
        links[i * (n-1) + (n-2 - j)] = c_adj[i][j];
      assert(degrees[i]+c_degrees[i] == n-1);
    }
  }
};
